//
//  LoadingView.swift
//  FictituousBankMobile
//
//  Created by chibundu Anwuna on 2019-02-03.
//  Copyright © 2019 NormBreakers. All rights reserved.
//
 
import UIKit

/**
 
 This class is responsible for showing a view that indicates a network operation is being performed.
 */
class LoadingView {
    private var subView: UIView!
    private var activityIndicator: UIActivityIndicatorView!
    
    
    class var shared: LoadingView {
        struct Static {
            static  let instance: LoadingView = LoadingView()
        }
        return Static.instance
    }
    
    private init(){
    }
    
    /**
     This method is used to start the activity indicator
     */
    func startAnimating(in view: UIView){
        subView = createLoadingView(in: view)
        view.addSubview(subView)
        view.addConstraints([
            subView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            subView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            subView.topAnchor.constraint(equalTo: view.topAnchor),
            subView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
            ])
        activityIndicator.startAnimating()
    }
    
    /**
     This method is used to stop the activity indicator
     */
    func stopAnimating() {
        activityIndicator?.stopAnimating()
        subView?.removeFromSuperview()
    }
    
    private func createLoadingView(in view: UIView) -> UIView{
        let loadingView = UIView(frame: CGRect(x: view.frame.origin.x, y: view.frame.origin.y, width: view.frame.width, height: view.frame.height))
        self.activityIndicator = UIActivityIndicatorView()
        
        loadingView.backgroundColor = UIColor(white: 0, alpha: 0.2)
        loadingView.clipsToBounds = true
        loadingView.layer.zPosition = 1
        activityIndicator.style = .white
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        loadingView.addSubview(activityIndicator)
        
        loadingView.addConstraints([
            NSLayoutConstraint(item: activityIndicator, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 40),
            NSLayoutConstraint(item: activityIndicator, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 40),
            NSLayoutConstraint(item: activityIndicator, attribute: .centerX, relatedBy: .equal, toItem: loadingView, attribute: .centerXWithinMargins, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: activityIndicator, attribute: .centerY, relatedBy: .equal, toItem: loadingView, attribute: .centerYWithinMargins, multiplier: 1, constant: 0)
            ])
        
        return loadingView
    }
}

//
//  AccountSummaryTableViewCell.swift
//  FictituousBankMobile
//
//  Created by chibundu Anwuna on 2019-01-29.
//  Copyright © 2019 NormBreakers. All rights reserved.
//

import UIKit

class AccountSummaryTableViewCell: UITableViewCell {

    @IBOutlet weak var accountNameLabel: UILabel!
    @IBOutlet weak var accountNumberLabel: UILabel!
    @IBOutlet weak var balanceLabel: UILabel!
    
    lazy var formatter: NumberFormatter = {
        var formatter = NumberFormatter()
        formatter.locale = Locale.current
        formatter.numberStyle = .currency
        return formatter
    }()
    
    func configurCell(with account: Account){
        accountNameLabel.text = account.accountName
        accountNumberLabel.text = account.id
        balanceLabel.text = formatter.string(from: account.balance as NSNumber)
    }
    
}

//
//  AccountManager.swift
//  FictituousBankMobile
//
//  Created by chibundu Anwuna on 2019-02-03.
//  Copyright © 2019 NormBreakers. All rights reserved.
//

import Foundation

class AccountManager: NSObject {
    
    struct Account {
        static let username = "Username"
    }
    
    static func getUsername() -> String? {
        return UserDefaults.standard.string(forKey: Account.username)
    }
    
    static func set(username: String? = nil) {
        UserDefaults.standard.set(username, forKey: Account.username)
    }
    
}

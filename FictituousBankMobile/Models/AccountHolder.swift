//
//  AccountHolder.swift
//  FictituousBankMobile
//
//  Created by chibundu Anwuna on 2019-01-30.
//  Copyright © 2019 NormBreakers. All rights reserved.
//

import Foundation

struct Login: Codable {
    let userName, password: String
}


struct AccountHolder: Codable {
    let userName, firstName, lastName: String
    let accounts: [Account]
}

struct Account: Codable {
    let id, accountName,accountTypeId, accountTypeName: String
    let balance: Double
}

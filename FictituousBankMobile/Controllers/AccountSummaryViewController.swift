//
//  AccountSummaryViewController.swift
//  FictituousBankMobile
//
//  Created by chibundu Anwuna on 2019-01-29.
//  Copyright © 2019 NormBreakers. All rights reserved.
//

import UIKit

class AccountSummaryViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var accounts: [Account]!
    
    let sectionHeaderHeight: CGFloat = 50
    var accountSummary = [String: [Account]]()
    var accountTypes = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "AccountSummaryTableViewCell", bundle: nil), forCellReuseIdentifier: "Cell")
        sortAccount()
    }
    
    func sortAccount() {
        for account in accounts {
            if let _ = accountSummary[account.accountTypeName] {
                accountSummary[account.accountTypeName]!.append(account)
            }else {
                accountSummary[account.accountTypeName] = [account]
            }
        }
        
        accountTypes = Array(accountSummary.keys.sorted())
    }

    @IBAction func logout(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension AccountSummaryViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return accountTypes.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return accountSummary[accountTypes[section]]?.count ?? 0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! AccountSummaryTableViewCell
        
        let sectionName = accountTypes[indexPath.section]
        if let account = accountSummary[sectionName]?[indexPath.row] {
            cell.configurCell(with: account)
        }
        return cell
    }
    
    
    // delegate methods
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        // only show a section if the section is available
        let sectionName = accountTypes[section]
        if let accounts = accountSummary[sectionName], accounts.count > 0{
            return sectionHeaderHeight
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionName = accountTypes[section]
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: sectionHeaderHeight))
        
        let label = UILabel(frame: CGRect(x: 15, y: 0, width: tableView.bounds.width - 30, height: sectionHeaderHeight))
        label.textColor = UIColor.black
        label.font = UIFont.systemFont(ofSize: 20)
        label.text = sectionName
        
        view.addSubview(label)
        return view
        
    }
    
}

//
//  LoginViewController.swift
//  FictituousBankMobile
//
//  Created by chibundu Anwuna on 2019-01-29.
//  Copyright © 2019 NormBreakers. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var rememberCardSwitch: UISwitch!
    
    @IBOutlet weak var invalidUsernameLabel: UILabel!
    @IBOutlet weak var invalidPasswordLabel: UILabel!
    
    var segueIdentifier = "PresentAccountSummarySegue"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        usernameTextField.text = AccountManager.getUsername()
    }
    
    func allTextFieldsAreValid() -> Bool {
        guard let username = usernameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines), !username.isEmpty
            else {
                invalidUsernameLabel.isHidden = false
                usernameTextField.layer.borderColor = UIColor.red.cgColor
                return false
            }
        invalidUsernameLabel.isHidden = true
        usernameTextField.layer.borderColor = UIColor.lightGray.cgColor
        
        guard let password = passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines), !password.isEmpty else {
            invalidPasswordLabel.isHidden = false
            usernameTextField.layer.borderColor = UIColor.red.cgColor
            return false
        }
        invalidPasswordLabel.isHidden = true
        passwordTextField.layer.borderColor = UIColor.lightGray.cgColor
    
        return true
    }
    
    @IBAction func login(_ sender: UIButton) {
        
        guard allTextFieldsAreValid() else {
            return
        }
        let username = usernameTextField.text!
        let password = passwordTextField.text!
        let login = Login(userName: username, password: password)
        
        LoadingView.shared.startAnimating(in: self.view)
        APIManager.shared.login(with: login, onSuccess: { accountHolder in
            LoadingView.shared.stopAnimating()
            
            if self.rememberCardSwitch.isOn {
                AccountManager.set(username: username)
            }else {
                AccountManager.set()
            }
            self.performSegue(withIdentifier: "PresentAccountSummarySegue", sender: accountHolder.accounts)
            
        }) { error in
            LoadingView.shared.stopAnimating()
            let alertController = UIAlertController(title: "Login failed", message: error.rawValue, preferredStyle: .alert)
            let dismissAction = UIAlertAction(title: "OK", style: .default)
            alertController.addAction(dismissAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "PresentAccountSummarySegue" {
            let navigationController = segue.destination as! UINavigationController
            let destination = navigationController.topViewController as! AccountSummaryViewController
            destination.accounts = sender as? [Account]
        }
    }
    

}

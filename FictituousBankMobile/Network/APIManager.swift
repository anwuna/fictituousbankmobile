//
//  APIManager.swift
//  FictituousBankMobile
//
//  Created by chibundu Anwuna on 2019-01-30.
//  Copyright © 2019 NormBreakers. All rights reserved.
//

import Foundation

/**
 This class is reponsible for making HTTP requests to the server.
 */
class APIManager {
    let baseURL = "https://fictitiousbankservice.azurewebsites.net"
    let loginEndpoint = "/api/login"
    
    static let shared = APIManager()
    
    func login(with login: Login, onSuccess: @escaping(AccountHolder) -> Void, onFailure: @escaping(Error) -> Void) {
        let urlString = baseURL + loginEndpoint
        
        guard let url = URL(string: urlString) else {
            onFailure(Error.unableToLogin)
            return
        }
        let jsonEncoder = JSONEncoder()
        let jsonData = try? jsonEncoder.encode(login)
        
        var request = URLRequest(url: url)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        request.httpBody = jsonData
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            DispatchQueue.main.async {
                guard error == nil else {
                    onFailure(Error.unableToLogin)
                    return
                }
                
                // if unable to read data return error
                guard let data = data else {
                    onFailure(Error.serverError)
                    return
                }
                
                // if server response is with the 4XX series. Display generic message to user
                if let response = response as? HTTPURLResponse,
                    (400...499).contains(response.statusCode) {
                        onFailure(Error.invalidUsernameOrPassword)
                        return
                }
                
                let jsonDecoder = JSONDecoder()
                do {
                    let accountHolder = try jsonDecoder.decode(AccountHolder.self, from: data)
                    onSuccess(accountHolder)
                } catch {
                    onFailure(Error.serverError)
                    print(error)
                }
            }
        }
        
        task.resume()
    }

}

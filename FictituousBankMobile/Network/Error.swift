//
//  Error.swift
//  FictituousBankMobile
//
//  Created by chibundu Anwuna on 2019-02-03.
//  Copyright © 2019 NormBreakers. All rights reserved.
//

import Foundation

enum Error: String {
    case invalidUsernameOrPassword = "Invalid username or password"
    case serverError = "Unable to login at this time"
    case unableToLogin = "Login failed"
}

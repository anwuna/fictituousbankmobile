# Fictitious Bank Mobile
This app makes a web service call to validate a user's credentials and return a fictitious account summary.  Valid logins are: 

| Username | Password  |
|--|--|
| Carson |  password  |
| Meredith |  password  |
| Arturo |  password  |
| Yan |  password  |

